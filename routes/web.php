<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/todo');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/todo', 'TodoController@index')->name('todo');
Route::get('/todo/create', 'TodoController@create')->name('todo.create');
Route::post('/todo/store', 'TodoController@store')->name('todo.store');
Route::get('/todo/delete/{todo}', 'TodoController@destroy')->name('todo.delete');
