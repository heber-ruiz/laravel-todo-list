@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (isset($notificacion))
            <div class="alert alert-info">
                <ul>
                        <li>{{ $notificacion }}</li>
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left m-1 mt-2">Lista Todo</h3>
                    <a href="{{url('/todo/create')}}" class="btn btn-success mx-1 my-1 float-right">Agregar Todo</a>
                </div>

                <ul class="list-group list-group-flush">
                    @foreach ( $todos as $todo)
                        <li class="list-group-item"> 
                            <h3>{{$todo->titulo}}</h3> 
                            <p>{{$todo->descripcion}}</p>
                            <a href="{{url('/todo/delete/'.$todo->id)}}" class="btn btn-danger mx-1 my-1 float-right">Borrar Todo</a>
                        </li>
                    @endforeach          
                </ul> 
            </div>
            
              
        </div>
    </div>
</div>
@endsection