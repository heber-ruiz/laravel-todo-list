@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left m-1 mt-2">Agregar Todo</h3>
                </div>
                <form action="{{ route('todo.store') }}" method="POST">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="form-group">
                            <label for="tituloInput">Título del Todo</label>
                            <input type="text" class="form-control" id="tituloInput" name="tituloInput">
                        </div>
                        <div class="form-group">
                            <label for="descripcionInput">Descripción del Todo</label>
                            <textarea class="form-control" id="descripcionInput" name="descripcionInput" rows="4"> </textarea>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <button type="submit" class="btn btn-success m-1 float-right">Crear Todo</button>
                        <a href="{{url("/todo")}}" class="btn btn-danger m-1 float-right">Cancelar</a>
                    </div>
                </form>
            </div>         
        </div>
    </div>
</div>
@endsection